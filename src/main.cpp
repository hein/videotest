/*
 * Copyright 2019 Eike Hein <hein@kde.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License or (at your option) version 3 or any later version
 * accepted by the membership of KDE e.V. (or its successor approved
 * by the membership of KDE e.V.), which shall act as a proxy
 * defined in Section 14 of version 3 of the license.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <KAboutData>
#ifndef Q_OS_ANDROID
#include <KCrash>
#endif
#include <KLocalizedContext>
#include <KLocalizedString>

#ifdef Q_OS_ANDROID
#include <QGuiApplication>
#else
#include <QApplication>
#endif

#include <QCommandLineParser>
#include <QIcon>
#include <QQmlApplicationEngine>
#include <QQmlContext>
#include <QQmlEngine>
#include <QQuickView>

#include <VLCQtCore/Common.h>
#include <VLCQtQml/Qml.h>

#ifdef Q_OS_ANDROID
Q_DECL_EXPORT
#endif
int main(int argc, char *argv[])
{
    QGuiApplication::setAttribute(Qt::AA_EnableHighDpiScaling);

#ifdef Q_OS_ANDROID
    QGuiApplication app(argc, argv);
#else
    QApplication app(argc, argv);
#endif

    KLocalizedString::setApplicationDomain("videotest");

    KAboutData aboutData(QStringLiteral("videotest"),
        xi18nc("@title", "<application>VideoTest</application>"),
        QStringLiteral("0.1"),
        xi18nc("@title", ""),
        KAboutLicense::GPL,
        xi18nc("@info:credit", "(c) 2019 The VideoTest Team"),
        QString(),
        QStringLiteral("")); // FIXME Website URL

    aboutData.setOrganizationDomain(QByteArray("kde.org"));
    aboutData.setProductName(QByteArray("videotest"));

    KAboutData::setApplicationData(aboutData);
    QCommandLineParser parser;
    parser.addHelpOption();
    parser.addVersionOption();

    aboutData.setupCommandLine(&parser);
    parser.process(app);
    aboutData.processCommandLine(&parser);

    app.setApplicationName(aboutData.componentName());
    app.setApplicationDisplayName(aboutData.displayName());
    app.setOrganizationDomain(aboutData.organizationDomain());
    app.setApplicationVersion(aboutData.version());
    app.setWindowIcon(QIcon::fromTheme(QStringLiteral("uav"))); // FIXME: Icon name.

#ifndef Q_OS_ANDROID
    KCrash::initialize();
#endif

    VlcCommon::setPluginPath(app.applicationDirPath() + "/plugins");
    VlcQml::registerTypes();

    QQmlApplicationEngine engine(&app);


    // For i18n.
    engine.rootContext()->setContextObject(new KLocalizedContext(&engine));

    engine.rootContext()->setContextProperty(QStringLiteral("videotestAboutData"),
        QVariant::fromValue(KAboutData::applicationData()));

    engine.load(QUrl(QStringLiteral("qrc:///main.qml")));

    return app.exec();
}
